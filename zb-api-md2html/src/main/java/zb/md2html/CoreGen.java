package zb.md2html;

import java.io.IOException;
import java.util.HashMap;

import com.jfinal.kit.PathKit;

import cn.hutool.core.collection.CollUtil;
import jodd.io.FileUtil;
import jodd.jerry.Jerry;
import jodd.template.MapTemplateParser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import zb.md2html.entity.Language;
import zb.md2html.gen.ErrorCode;
import zb.md2html.gen.H1;
import zb.md2html.gen.Info;
import zb.md2html.gen.ZbBase;

/**
 * 所有html的组装
 * 
 * @author Administrator
 *
 */
@RequiredArgsConstructor
public class CoreGen extends ZbBase{

	private @NonNull String projectPath;
	private final String template = PathKit.getRootClassPath() + "/template/root.template";

	public void codeGen() throws IOException {
		val htmlH1 = new H1(projectPath).getApiContent();
		val htmlErrorCode = new ErrorCode().getApiContent();
		val params = new HashMap<String,String>();
		val htmlInfo = new Info(PathKit.getRootClassPath() + "/info/").getApiContent();
		// 生成html主体文件
		// 第一部分的简介
		params.put("info", htmlInfo);
		
		val htmlFront = new Info(PathKit.getRootClassPath() + "/front/").getApiContent();
		
		params.put("htmlFront", htmlFront);
		
		// 所有h1的主体内容
		params.put("content", htmlH1);
		// 返回码
		params.put("errorCode", htmlErrorCode);
		//模板代码标签支持
		Language lans = super.getLans();
		params.put("lan_line", CollUtil.join(lans.getLans(), "','"));
		params.put("lan_html", lans.getHtml());
		params.put("after", FileUtil.readString(afterTemplate));

		String html = new MapTemplateParser().of(params).parse(FileUtil.readString(template));
		// 特殊字符转义
		html = html.replaceAll("&currency=", "&amp;currency=");
		html = html.replaceAll("&price=", "&amp;price=");
		html = html.replaceAll("&tradeType=", "&amp;tradeType=");

		val doc = Jerry.jerry(html);
		doc.$("a").attr("target", "_Blank");
		html = doc.html();
		
		
		
		
		

		// 生成翻译文件
		html = new GenCn2i18n(projectPath, html).gen();
		System.out.println("生成完成!!!");

		// 最后 持久化html文件到本地磁盘
		FileUtil.writeString(projectPath + "views\\cn\\api\\help.html", html);
	}
	
	
}
