## 获取历史成交
```request
GET http://api.zb.cn/data/v1/trades?market=btc_usdt
```
```response
[
    {
        "amount": 0.541,
        "date": 1472711925,
        "price": 81.87,
        "tid": 16497097,
        "trade_type": "ask",
        "type": "sell"
    }...
]
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
market | String | 例：btc_qc
since | long | 从指定交易ID后50条数据

```resdata
date : 交易时间(时间戳)
price : 交易价格
amount : 交易数量
tid : 交易生成ID
type : 交易类型，buy(买)/sell(卖)
trade_type : 委托类型，ask(卖)/bid(买)
```

```java
public void getTrades() {
	//得到返回结果
	String returnJson = HttpRequest.get("http://api.zb.cn/data/v1/trades?market=btc_usdt").send().bodyText();
}
```

```python
def get(self, url):
    while True:
        try:
            r = requests.get(url)
        except Exception:
            time.sleep(0.5)
            continue
        if r.status_code != 200:
            time.sleep(0.5)
            continue
        r_info = r.json()
        r.close()
        return r_info
        
def getTrades(self, market):
    url = 'http://api.zb.cn/data/v1/trades?market=btc_usdt'
    return self.get(url)
```