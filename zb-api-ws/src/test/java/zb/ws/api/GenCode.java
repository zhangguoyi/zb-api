package zb.ws.api;

import org.junit.Before;
import org.junit.Test;

import entity.commons.UserApiEntity;
import entity.enumtype.ExchangeEnum;
import lombok.val;

public class GenCode {
	private final String apiKey = "ce2a18e0-dshs-4c44-4515-9aca67dd706e";
	private final String secretKey = "c11a122s-dshs-shsa-4515-954a67dd706e";
	private final String symbol= "usdtqc";
	
	private WsZb api;
	
	@Test
	public void gen() throws Exception {
//		api.getAccount();
//		api.buy(symbol, 1, 0.001);
//		api.getOrder(symbol, 20180522105585216l, "测试");
//		api.cancel(symbol, 20180522105585216l, "取消");
//		api.getOrders(1, 1, symbol);
//		api.getOrdersIgnoreTradeType(1, 10, symbol);
		
//		api.getUnfinishedOrdersIgnoreTradeType(1, 10, symbol);
//		api.getOrdersNew(1, 10, 1, symbol);
		
		api.getLeverAssetsInfo();//内部错误
		api.getLeverBills("qc");//内部错误
		api.transferInLever();
		api.transferOutLever();//内部错误
		api.loan();//内部错误
		api.cancelLoan();
		api.getLoans();
		api.getLoanRecords();
		api.borrow();
		api.repay();
		api.getRepayments();
		api.getFinanceRecords();
//		
//		api.addSubUser();
//		api.createSubUserKey();
//		api.getSubUserList();
//		api.doTransferFunds();
	}
	
	@Before
	public void before() {
		val userApi = new UserApiEntity(ExchangeEnum.zb, "测试", apiKey,secretKey);
		api = new WsZb(userApi);
	}
}
